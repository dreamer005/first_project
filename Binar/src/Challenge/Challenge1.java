package Challenge;
import java.util.Scanner;
// Alfin Sevia Rahman
// BEJ2202KM059
// Challenge1


public class Challenge1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean exit = false;
		do {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("\n\n\n\n---------------------------------------------");
		System.out.println("Kalkulator Penghitung Luas dan Volume ");
		System.out.println("---------------------------------------------");
		System.out.println("Menu");
		System.out.println("1. Hitung Luas Bidang");
		System.out.println("2. Hitung Volume");
		System.out.println("0. Tutup Aplikasi");
		System.out.print("Pilih Operasi Kalkulator : ");
		int pilihan = keyboard.nextInt();
		System.out.println("---------------------------------------------");
		
		
		switch (pilihan) {
		case 1 :
			int panjang, lebar, r, alas, tinggi, sisi;
			Scanner in = new Scanner(System.in);
			System.out.println("\n---------------------------------------------");
			System.out.println("Pilih bidang yang akan dihitung ");
			System.out.println("---------------------------------------------");
			System.out.println("1. persegi");
			System.out.println("2. lingkaran");
			System.out.println("3. segitiga");
			System.out.println("4. persegi panjang");
			System.out.println("0. Kembali ke menu sebelumnya");
			System.out.print("Pilih Luas : ");
			int pilih = in.nextInt();
			System.out.println("---------------------------------------------");
		
				switch (pilih) {
				case 1 :
					Scanner kp = new Scanner(System.in);
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih persegi ");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan sisi : ");
					sisi = kp.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nLuas dari persegi adalah : " + sisi*sisi);
					System.out.println("---------------------------------------------");
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 2 :
					Scanner kl = new Scanner(System.in);
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih lingkaran ");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan Jari-Jari : ");
					r = kl.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nLuas dari lingkaran adalah : " + 3.14*r*r);
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 3 :
					Scanner ks = new Scanner(System.in);
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih segitiga ");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan Alas : ");
					alas = ks.nextInt();
					System.out.print("Masukan Tinggi : ");
					tinggi = ks.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nLuas dari Segitiga adalah : " + alas*tinggi / 2);
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 4 :
					Scanner kpp = new Scanner(System.in);
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih persegi panjang ");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan panjang : ");
					panjang = kpp.nextInt();
					System.out.print("Masukan lebar : ");
					lebar = kpp.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nLuas dari Segitiga adalah : " + panjang*lebar);
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 0 :
					try {System.in.read();}
					catch(Exception e) {}
					break;
			
				default :
					System.out.println("Masukan angka yang benar !");
					break;
				}
			break;
		case 2 :
			
			System.out.println("\n---------------------------------------------");
			System.out.println("Pilih bangun yang akan di hitung");
			System.out.println("---------------------------------------------");
			System.out.println("1. Kubus");
			System.out.println("2. Balok");
			System.out.println("3. Tabung");
			System.out.println("0. Kembali ke menu sebelumnya");
			System.out.print("Pilih bangun : ");
			Scanner put = new Scanner(System.in);
			int milih = put.nextInt();
			System.out.println("---------------------------------------------");
				switch (milih) {
				case 1 :
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih Kubus");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan Panjang sisi : ");
					Scanner vk = new Scanner(System.in);
					int sv = vk.nextInt();
					System.out.print("\nprocessing . . .");
					System.out.println("\nVolume dari kubus adalah : " + sv*sv*sv);
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 2 :
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih Balok");
					System.out.println("---------------------------------------------");
					System.out.print("Masukan Panjang : ");
					Scanner vb = new Scanner(System.in);
					int pb = vb.nextInt();
					System.out.print("Masukan Lebar : ");
					int lb = vb.nextInt();
					System.out.print("Masukan Tinggi : ");
					int tb = vb.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nVolume dari balok adalah : " + pb*lb*tb );
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 3 :
					System.out.println("\n---------------------------------------------");
					System.out.println("Anda memilih Tabung");
					System.out.println("---------------------------------------------");
					Scanner vt = new Scanner(System.in);
					System.out.print("Masukan jari-jari : ");
					int jt = vt.nextInt();
					System.out.print("Masukan tinggi : ");
					int tt = vt.nextInt();
					System.out.println("\nprocessing . . .");
					System.out.println("\nVolume dari tabung adalah : " + 3.14*jt*tt );
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				case 0 :
					System.out.println("tekan apa saja untuk kembali ke menu utama");
					try {System.in.read();}
					catch(Exception e) {}
					break;
				default :
					System.out.println("Masukan angka yang benar !");
					break;
					
				}
			break;
		case 0 :
			System.exit(0);
		default :
			System.out.println("Masukan angka yang benar !");
			break;

		}
		} while (!exit);
	}

}
